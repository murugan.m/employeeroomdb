package com.example.FireBase

import com.google.firebase.database.IgnoreExtraProperties


@IgnoreExtraProperties
data class EmployeeFireBase(val name: String? = null, val phone: String? = null, val email: String? = null, val idValue: String? = null) {
    // Null default values create a no-argument default constructor, which is needed
    // for deserialization from a DataSnapshot.
}