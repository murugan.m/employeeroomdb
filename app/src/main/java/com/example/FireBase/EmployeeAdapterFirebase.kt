package com.example.FireBase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.employeedb.Employee
import com.example.employeedb.EmployeeDataBase
import com.example.employeedb.OnItemClick
import com.example.employeedb.R

interface OnItemClickFireBase {
    fun onDeleteClick(emailId: String?)
    fun didSelectEmployee(employee: EmployeeFireBase)
}

internal class EmployeeAdapterFirebase(private var itemList: List<EmployeeFireBase>, onItemClick: FirebaseActivity) :  RecyclerView.Adapter<EmployeeAdapterFirebase.MyViewHolder>() {
    var onItemClickValue: OnItemClickFireBase = onItemClick
    internal inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.findViewById(R.id.nameId)
        var phone: TextView = view.findViewById(R.id.phoneId)
        var email: TextView = view.findViewById(R.id.emailId)
        var delbtn: Button = view.findViewById(R.id.deleteBtnId)
        var editBtn: Button = view.findViewById(R.id.editBtnId)
        var layout: ConstraintLayout = view.findViewById(R.id.linearLayout)
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_employee, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = itemList[position]
        holder.name.text = item.name
        holder.phone.text = item.phone
        holder.email.text = item.email

        holder.delbtn.setOnClickListener{
            onItemClickValue.onDeleteClick(item.idValue)
        }

        holder.editBtn.setOnClickListener{
            onItemClickValue.didSelectEmployee(item)
        }

        holder.layout.setOnClickListener {
            print("Clicked on $item")
//            onItemClickValue.didSelectEmployee(item)
        }
    }

    override fun getItemCount(): Int {
        return itemList.size
    }
}