package com.example.FireBase

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.helper.widget.MotionEffect.TAG
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.employeedb.*
import com.example.employeedb.databinding.ActivityFirebaseBinding
import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

lateinit var fireDatabase: DatabaseReference
class FirebaseActivity : AppCompatActivity(), OnItemClickFireBase {
    lateinit var binding: ActivityFirebaseBinding
    private val itemsList = ArrayList<EmployeeFireBase>()
    private lateinit var employeeAdapter : EmployeeAdapterFirebase
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFirebaseBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val recyclerView: RecyclerView = binding.listViewId

        employeeAdapter = EmployeeAdapterFirebase(itemsList, this)
        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = employeeAdapter

        fireDatabase = Firebase.database.reference

//        val user = EmployeeFireBase("Name", "782738723", "ammssmm@gmil.com")
//
//        database.child("employeeFireBase").child("${user.name}${user.phone}").setValue(user)
//
//        val user5 = EmployeeFireBase("Name9", "782738723", "ammssmm@gmil.com")
//
//        database.child("employeeFireBase").child("${user5.name}${user5.phone}").setValue(user)
//
//
//        val user4 = EmployeeFireBase("Name7", "782738723", "ammssmm@gmil.com")
//
//        database.child("employeeFireBase").child("${user4.name}${user4.phone}").setValue(user)
//
//
//        val user3 = EmployeeFireBase("Name6", "782738723", "ammssmm@gmil.com")
//
//        database.child("employeeFireBase").child("${user3.name}${user3.phone}").setValue(user)
//
//
//        val user2 = EmployeeFireBase("Name4", "782738723", "ammssmm@gmil.com")
//
//        database.child("employeeFireBase1").child("${user2.name}${user2.phone}").setValue(user)
//
//
//        val user1 = EmployeeFireBase("Name2", "782738723", "ammssmm@gmil.com")
//
//        database.child("employeeFireBase").child("${user1.name}${user1.phone}").setValue(user)
//

        binding.addBtnId.setOnClickListener {
            isFireBase = true
            val intent = Intent(this, AddEmployeeDetails::class.java)
            startActivity(intent)
        }


        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                // Get Post object and use the values to update the UI
                val post = dataSnapshot.getValue<EmployeeFireBase>()
                print(post)
                Log.w(TAG, "loadPost:onCancelled $post ")
                getEmployeeData()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException())
            }
        }
        fireDatabase.addValueEventListener(postListener)

    }

    override fun onStart() {
        super.onStart()
        getEmployeeData()

    }

    override fun onDeleteClick(emailId: String?) {
        fireDatabase.child("employee").child("$emailId").removeValue().addOnSuccessListener {
            ContextCompat.getMainExecutor(this@FirebaseActivity).execute {
                showAlert("Succcess", "Entry deleted" , completion = {
                    getEmployeeData()
                })
            }
        }
            .addOnFailureListener {
                ContextCompat.getMainExecutor(this@FirebaseActivity).execute {
                    showAlert("Failed", "Entry not deleted \n try again later" , {
                        getEmployeeData()
                    })
                }
            }
    }

    override fun didSelectEmployee(employee: EmployeeFireBase) {
        isFireBase = true
        val intent = Intent(this, AddEmployeeDetails::class.java)
        intent.putExtra("name",employee.name)
        intent.putExtra("phone",employee.phone)
        intent.putExtra("email",employee.email)
        intent.putExtra("idValue",employee.idValue)
        intent.putExtra("edit", true)
        startActivity(intent)
    }

    fun getEmployeeData() {
        fireDatabase.child("employee").get().addOnSuccessListener {
            itemsList.clear()
            Log.i("firebase", it.value.toString())
            val gson = Gson()
            val s1: String = gson.toJson(it.getValue())
            var objectValue: JSONObject? = null
            try {
                objectValue = JSONObject(s1)
                Log.i("firebase", "Got value $objectValue keys ${objectValue.keys()}")
                for (key in objectValue.keys()) {
                    val post = objectValue.getJSONObject(key)
                    var testModel = gson.fromJson(post.toString(), EmployeeFireBase::class.java)
                    if (testModel != null) {
                        itemsList.add(testModel)
                    }
                    Log.i("firebase", "Got value easch $testModel")
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
            employeeAdapter.notifyDataSetChanged();
        }.addOnFailureListener{
            Log.e("firebase", "Error getting data", it)
        }
    }

    fun showAlert(title: String = "Alert", messsage:String, completion: ((String) -> Unit?)? = null)  {
        val builder = AlertDialog.Builder(this@FirebaseActivity)
        builder.setTitle(title)
        builder.setMessage(messsage)
//builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

        builder.setPositiveButton("Ok") { dialog, which ->
            if (completion != null) {
                completion(title)
            }
        }
        builder.show()
    }
}
