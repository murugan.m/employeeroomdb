package com.example.employeedb

import android.os.Bundle
import android.text.InputType
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.FireBase.EmployeeFireBase
import com.example.FireBase.fireDatabase
import com.example.employeedb.databinding.ActivityAddEmployeeDetailsBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


class AddEmployeeDetails : AppCompatActivity() {
    var isEditableEmployee:Boolean = false
    var name:String = ""
    var phone:String = ""
    var email:String = ""
    var idValue: String = ""
    private lateinit var binding : ActivityAddEmployeeDetailsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddEmployeeDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle("Add Employee Details")
        isEditableEmployee = intent.getBooleanExtra("edit", false)
        name = intent.getStringExtra("name").toString()
        phone = intent.getStringExtra("phone").toString()
        email = intent.getStringExtra("email").toString()
        if(isEditableEmployee) {
            idValue = intent.getStringExtra("idValue").toString()
            setTitle("Update Employee Details")
            binding.employeeNameId.setText(name)
            binding.employeePhoneId.setText(phone)
            binding.employeeEmailId.setText(email)
            binding.employeeEmailId.inputType = InputType.TYPE_NULL
        }
        binding.saveBtnId.setOnClickListener {
            if (validateEmployee()) {
                if (isFireBase) {
                    if (!isEditableEmployee) {
                        var uniqueID = UUID.randomUUID().toString()
                        Log.i("firebase", "Got uniqueID $uniqueID")

                        val employee = EmployeeFireBase("${binding.employeeNameId.text}", "${binding.employeePhoneId.text}", "${binding.employeeEmailId.text}", "$uniqueID")
                        fireDatabase.child("employee").child("$uniqueID").setValue(employee).addOnSuccessListener {
                            ContextCompat.getMainExecutor(this@AddEmployeeDetails).execute {
                                showAlert("Success", "Values are updated successfully",
                                    {
                                        cancelBtnAction()
                                    })
//                            resetVallues()
                            }
                        }
                            .addOnFailureListener {
                                ContextCompat.getMainExecutor(this@AddEmployeeDetails).execute {
                                    showAlert("failure", it.toString(),
                                        {
                                            cancelBtnAction()

                                        })
//                            resetVallues()
                                }
                            }
                    } else {
                        val employee = EmployeeFireBase("${binding.employeeNameId.text}", "${binding.employeePhoneId.text}", "${binding.employeeEmailId.text}", "$idValue")
                        fireDatabase.child("employee").child("$idValue").setValue(employee).addOnSuccessListener {
                            ContextCompat.getMainExecutor(this@AddEmployeeDetails).execute {
                                showAlert("Success", "Values are stored successfully",
                                    {
                                        cancelBtnAction()

                                    })
//                            resetVallues()
                            }
                        }
                            .addOnFailureListener {
                                ContextCompat.getMainExecutor(this@AddEmployeeDetails).execute {
                                    showAlert("Failure", it.toString(),
                                        {
                                            cancelBtnAction()

                                        })
//                            resetVallues()
                                }
                            }
                    }

                } else {
                    GlobalScope.launch {

                        val employee = database?.employeeDAO()?.getEmployeeFor("${binding.employeeEmailId.text}")
                        if (employee == null || isEditableEmployee) {
                            if (isEditableEmployee) {
                                if (employee != null) {
                                    Log.d("Employee", "Entry is inside Update ${Employee(employee.id,"${binding.employeeNameId.text}", "${binding.employeePhoneId.text}", "${binding.employeeEmailId.text}")}")
                                    database?.employeeDAO()?.updateEmployee(Employee(employee.id,"${binding.employeeNameId.text}", "${binding.employeePhoneId.text}", "${binding.employeeEmailId.text}"))
                                }
                            } else {
                                database?.employeeDAO()?.insertEmployee(Employee(0,"${binding.employeeNameId.text}", "${binding.employeePhoneId.text}", "${binding.employeeEmailId.text}"))
                            }
                            ContextCompat.getMainExecutor(this@AddEmployeeDetails).execute {
                                showAlert("Success", "Values are stored successfully",
                                    {
                                        cancelBtnAction()

                                    })
//                            resetVallues()
                            }
                        } else {
                            Log.d("Employee", "Entry is alreay there $employee")
                            ContextCompat.getMainExecutor(this@AddEmployeeDetails).execute {
                                showAlert("Warning", "Entry for \"${binding.employeeEmailId.text}\" already available")
                            }
                        }
                    }
                }
            } else {
                showAlert("Alert", "Please file all the fields")
            }
        }

        binding.cancelBtnId.setOnClickListener {
            cancelBtnAction()
        }

    }

    private fun validateEmployee() : Boolean {
        if (binding.employeeNameId.length() <= 0) {
            return  false
        }
        if (binding.employeeEmailId.length() <= 0) {
            return  false
        }

        if (binding.employeePhoneId.length() <= 0) {
            return false
        }
        return  true
    }

    fun showAlert(title: String = "Alert", messsage:String, completion: ((String) -> Unit?)? = null)  {
        val builder = AlertDialog.Builder(this@AddEmployeeDetails)
        builder.setTitle(title)
        builder.setMessage(messsage)
//builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

        builder.setPositiveButton("Ok") { dialog, which ->
            if (completion != null) {
                completion(title)
            }
        }
        builder.show()
    }

    fun cancelBtnAction() {
        this.finish()
    }

    fun resetVallues() {
        binding.employeeEmailId.setText("")
        binding.employeePhoneId.setText("")
        binding.employeeEmailId.setText("")
    }
}