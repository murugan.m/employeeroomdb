package com.example.employeedb

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.FireBase.FirebaseActivity
import com.example.employeedb.databinding.ActivityMainBinding

var isFireBase : Boolean = false
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle("Choose")

        binding.roomDBid.setOnClickListener {
            isFireBase = false
            val roomDBUpdate = Intent(this, EmployeeActivity::class.java)
            startActivity(roomDBUpdate)
        }

        binding.fireBaseId.setOnClickListener {
            isFireBase = true
            val roomDBUpdate = Intent(this, FirebaseActivity::class.java)
            startActivity(roomDBUpdate)
        }

    }

}

