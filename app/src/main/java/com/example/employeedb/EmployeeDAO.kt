package com.example.employeedb

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface EmployeeDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEmployee(employee: Employee)

    @Update
    fun updateEmployee(employee: Employee)

    @Delete
    fun deleteEmployee(employee: Employee)

    @Query(value = "SELECT * FROM employee")
    fun getEmployees() : LiveData<List<Employee>>

    @Query(value = "SELECT * FROM employee WHERE email=:email")
    fun getEmployeeFor(email:String) : Employee
}
