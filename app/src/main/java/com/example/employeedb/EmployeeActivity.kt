package com.example.employeedb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.room.Room
import com.example.employeedb.databinding.ActivityEmployeeBinding
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

lateinit var database: EmployeeDataBase

class EmployeeActivity : AppCompatActivity() , OnItemClick {
    private val itemsList = ArrayList<Employee>()

    private lateinit var binding: ActivityEmployeeBinding
    private lateinit var employeeAdapter : EmployeeAdapter
    lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmployeeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle("Employee Details")

        database = Room.databaseBuilder(
            applicationContext,
            EmployeeDataBase::class.java,
            "employee"
        ).build()

        val recyclerView: RecyclerView = binding.listViewId
        employeeAdapter = EmployeeAdapter(itemsList, this)
        val layoutManager = LinearLayoutManager(applicationContext)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = employeeAdapter

        binding.addBtnId.setOnClickListener {
            isFireBase = false
            val intent = Intent(this, AddEmployeeDetails::class.java)
            startActivity(intent)
        }
    }

    fun getEmployeeData() {
        database.employeeDAO().getEmployees().observe(this, {
            Log.d("Employee", it.toString())
            itemsList.clear()
            itemsList.addAll(it)
            employeeAdapter.notifyDataSetChanged();
        })

    }

    override fun onStart() {
        super.onStart()

        getEmployeeData()
    }

    override fun onDeleteClick(emailId: String?) {
        GlobalScope.launch {
            val employee = database?.employeeDAO()?.getEmployeeFor("$emailId")
            if (employee != null) {
                database?.employeeDAO()?.deleteEmployee(employee)
                ContextCompat.getMainExecutor(this@EmployeeActivity).execute {
                    showAlert("Succcess", "Entry for \"$emailId\" deleted")
                }
            } else {
                ContextCompat.getMainExecutor(this@EmployeeActivity).execute {
                    showAlert("Warning", "Entry for \"$emailId\" not available")
                }
            }
        }
    }

    override fun didSelectEmployee(employee: Employee) {
        isFireBase = false
        val intent = Intent(this, AddEmployeeDetails::class.java)
        intent.putExtra("name",employee.name)
        intent.putExtra("phone",employee.phone)
        intent.putExtra("email",employee.email)
        intent.putExtra("edit", true)
        startActivity(intent)
    }

    fun showAlert(title: String = "Alert", messsage:String, completion: ((String) -> Unit?)? = null)  {
        val builder = AlertDialog.Builder(this@EmployeeActivity)
        builder.setTitle(title)
        builder.setMessage(messsage)
//builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

        builder.setPositiveButton("Ok") { dialog, which ->
            if (completion != null) {
                completion(title)
            }
        }
        builder.show()
    }
}

